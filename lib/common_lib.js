var getVisibleElementBySelector = function(selector) {
    return selector.filter(function (elm) {
        return elm.isDisplayed().then(function (value) {
            return value;
        });
    }).first();
}


exports.getVisibleElementBySelector = getVisibleElementBySelector;
