var LoginPage = require("../pages/login_page.js");
var Library = require("../lib/common_lib.js");


describe('Exercise to the course work',function() {

    var loginPage = new LoginPage();

    beforeEach(function() {
//        browser.get('/');
        browser.get('https://login-alpha.mimecast.com/m/secure/login/?tkn=WVXYHQ7cA8nSKgxK0RS4nkdOIDqpeB5cYTr3ptKQOtRRUm0poN7PZ_cOXXqmu1ZSPxbDSpccdqyHcLr4Z0WYoUUaJR4MrVGHESpRXK3_Gyw#/login');
    });

    it('Case 1 :Verify the elements present in Login page',function() {

        // Verify email address field
        loginPage.isEmailAddressFieldEnabled();
        expect(loginPage.emailaddressField.isEnabled()).toBeTruthy();

        // Verify next button element is enabled
        loginPage.isNextButtonEnabled();
        expect(Library.getVisibleElementBySelector(loginPage.nextButton).isEnabled()).toBeFalsy();
    });

    describe('Case 2', function() {

        beforeEach(function() {
            // Enter email address
            loginPage.emailaddressField.sendKeys(email_address);

            // Press Next button to navigate
            Library.getVisibleElementBySelector(loginPage.nextButton).click();
        });

        it('Verify the following additional elements are present on the login page after a correct user address has been entered', function() {
            // Verify the password field is enabled
            loginPage.isPasswordFieldEnbaled();
            expect(loginPage.passwordField.isEnabled()).toBeTruthy();
        });

        it('Verify that ‘Login as a different user’ button returns the user to the initial login page',function() {
            // Verify the link text is as expected
            loginPage.getTextOfLoginAsDiffUserlink();
            expect(loginPage.loginAsDiffUserlink.getText()).toEqual('Log in as a different user.');
            // clicking log in as different user link to navigate to login page
            loginPage.loginAsDiffUserlink.click();

            // Verify if its navigated to login page
            loginPage.isNextButtonEnabled();
            expect(Library.getVisibleElementBySelector(loginPage.nextButton).isDisplayed()).toBeTruthy();

        });

        it('Verify that reset password navigates the user to the reset page',function() {
        
            // Verify the linktext is as expected
            loginPage.getTextOfforgotpasswordlink();
             expect(loginPage.forgotpasswordlink.getText()).toEqual('Forgot your password?');
            //clicking forgot password link to navigate to Reset page
            loginPage.forgotpasswordlink.click();

            // Verify emailaddressField element is enabled
            loginPage.isEmailAddressFieldEnabled();
            expect(loginPage.emailaddressField.isEnabled()).toBeTruthy();

            //Verify reset button element is enabled
            loginPage.isResetButtonEnabled();
            expect(Library.getVisibleElementBySelector(loginPage.resetButton).isEnabled()).toBeTruthy();

            // Verify the linktext is as expected
            loginPage.getTextOfNeverMindLink();
            expect(loginPage.neverMindLink.isEnabled()).toBeTruthy();
            expect(loginPage.neverMindLink.getText()).toEqual('Never mind, take me back to the login page.')

            loginPage.neverMindLink.click();

            // Verify the password field is enabled
            loginPage.isPasswordFieldEnbaled();
            expect(loginPage.passwordField.isEnabled()).toBeTruthy();

            // Verify login button element is enabled
            loginPage.islogInButtonEnabled();
            expect(Library.getVisibleElementBySelector(loginPage.logInButton).isEnabled()).toBeFalsy();
        });

     });

    it('Case 3: Verify that user cannot log into the Secure Messaging site with an invalid user address.',function() {
       //Enter email address
        loginPage.emailaddressField.sendKeys("invalid@gmail.com");

        // PressNext button
        Library.getVisibleElementBySelector(loginPage.nextButton).click();
        // Enter Password
        loginPage.passwordField.clear().sendKeys(password);
        Library.getVisibleElementBySelector(loginPage.logInButton).click();

        // Verify the error message
        loginPage.errMessageText.getText().then(function(result) {
            console.log('Error message as expected :',result);
        });
        expect(loginPage.errMessageText.getText()).toEqual("Invalid user name, password or permissions.");
    });

    it('Case 4: Verify that user cannot log into the Secure Messaging site with an invalid password.',function() {
       // Enter email address
        loginPage.emailaddressField.sendKeys(email_address);
        expect(Library.getVisibleElementBySelector(loginPage.nextButton).isEnabled()).toBeTruthy();
        // PressNext button
        Library.getVisibleElementBySelector(loginPage.nextButton).click();
        // Enter Password
        loginPage.passwordField.clear().sendKeys("wrongpwd");
        Library.getVisibleElementBySelector(loginPage.logInButton).click();

        // Verify the error message
        loginPage.errMessageText.getText().then(function(result) {
            console.log('Error message as expected :',result);
        });
        expect(loginPage.errMessageText.getText()).toEqual("Invalid user name, password or permissions.");
    });

    it('Case 5: Verify that user can log into the Secure Messaging site with an valid credentials.',function() {
        // Enter email address
        loginPage.emailaddressField.sendKeys(email_address);
        expect(Library.getVisibleElementBySelector(loginPage.nextButton).isEnabled()).toBeTruthy();
        // PressNext button
        Library.getVisibleElementBySelector(loginPage.nextButton).click();
        // Enter Password
        loginPage.passwordField.clear().sendKeys(password);
        Library.getVisibleElementBySelector(loginPage.logInButton).click();

        // Verify login button element is enabled
        loginPage.refreshButton.isDisplayed().then(function(result) {
            console.log('Refresh button visible status -  ', result);
        });
        expect(loginPage.refreshButton.isPresent()).toBeTruthy();
    });
});




