var Library = require("../lib/common_lib.js");

var LoginPage = function() {

    this.emailaddressField      = element(by.id('username'));
    this.nextButton             = element.all(by.css('[translate="BUTTON_NEXT_STEP"]'));
    this.passwordField          = element(by.model('appCtrl.password'));
    this.loginAsDiffUserlink    = element(by.css('button[translate="LINK_LOGIN_AS_DIFFERENT_USER"]'));
    this.forgotpasswordlink     = element(by.css('button[translate="LINK_FORGOT_PASSWORD"]'));
    this.resetButton            = element.all(by.css('[translate="FORGOT_PASSWORD_RESET_PASSWORD_BTN"]'));
    this.neverMindLink          = element(by.css('button[translate="FORGOT_PASSWORD_GO_TO_LOGIN_LNK"]'));
    this.logInButton            = element.all(by.css('[translate="BUTTON_LOGIN"]'));
    this.errMessageText         = element(by.binding("appCtrl.errorMessage"));
    this.refreshButton          = element(by.css("[tooltip='Refresh']"));


    this.isEmailAddressFieldEnabled = function() {
       this.emailaddressField.isEnabled().then(function(result) {
         console.log('Email address field enabled status - ', result);
        });
    };

    this.isNextButtonEnabled = function() {
        Library.getVisibleElementBySelector(this.nextButton).isEnabled().then(function(result) {
            console.log('Next button enabled status -  ', result);
        });
    };

    this.isPasswordFieldEnbaled = function() {
       this.passwordField.isEnabled().then(function(result) {
            console.log('password field enabled status - ', result);
       });
    };

    this.getTextOfLoginAsDiffUserlink = function() {
       this.loginAsDiffUserlink.getText().then(function(result) {
            console.log('link text is shown as - ', result);
       });
    };

    this.isResetButtonEnabled = function() {
       Library.getVisibleElementBySelector(this.resetButton).isEnabled().then(function(result) {
            console.log('Reset button field enabled status - ', result);
       });
    };

    this.getTextOfNeverMindLink = function() {
       this.neverMindLink.getText().then(function(result) {
            console.log('link text is shown as - ', result);
       });
    };

    this.islogInButtonEnabled = function() {
        Library.getVisibleElementBySelector(this.logInButton).isEnabled().then(function(result) {
            console.log('login  button enabled status -  ', result);
        });
    };

    this.getTextOfforgotpasswordlink = function() {
        this.forgotpasswordlink.getText().then(function(result) {
            console.log('link text is shown  as ',result);
        });
    };



};


module.exports = LoginPage;
