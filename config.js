// config file for course work
exports.config = {

    seleniumAddress: 'http://localhost:4444/wd/hub',


    baseUrl: 'https://login-alpha.mimecast.com/m/secure/login/?tkn=WVXYHQ7cA8nSKgxK0RS4nkdOIDqpeB5cYTr3ptKQOtRRUm0poN7PZ_cOXXqmu1ZSPxbDSpccdqyHcLr4Z0WYoUUaJR4MrVGHESpRXK3_Gyw#/login',

    specs: 'spec/authentication_spec.js',
    framework: 'jasmine2',

    //        // Asynchronous script  default time out 11s
    //        allScriptsTimeout: 15000,
    //        // waiting for angular page to load default time out 10s
    //        getPageTimeout: 10000,
    capabilities: {
            'browserName': 'chrome',
    },

    jasmineNodeOpts: {
        isVerbose: true,
        showColors: true,
        includeStackTrace: true,
        //Spec complete default timeout is 30s
        defaultTimeoutInterval: 50000,
    },

    onPrepare: './onPrepare/startup.js',

     params: {
        user: {
            email: "<VALID_MAIL_ADDRESS>",
            password: "<PASSWORD>",
        }
     }
};
