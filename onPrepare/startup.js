// Import neccessary libraries
var jasmineReporters = require('jasmine-reporters');
var HtmlScreenshotReporter = require('protractor-jasmine2-screenshot-reporter');

// Parse user data from config file
email_address = browser.params.user.email;
password = browser.params.user.password;

browser.manage().window().maximize();
// Implicit wait seconds - Waits for elements to appear on the page
browser.manage().timeouts().implicitlyWait(3000);

var reporter = new HtmlScreenshotReporter({
  dest: 'target/screenshots',
  filename: 'my-report.html',
  showSummary: true,
  showQuickLinks: true
});
jasmine.getEnv().addReporter(reporter);


// Report result on the terminal
jasmine.getEnv().addReporter(new jasmineReporters.TapReporter({
    consolidateAll: true,
}));






